import json
import os
import time

from kivy.app import App
from kivy.core.window import Window
from kivy.properties import StringProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.filechooser import FileChooserListView
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.textinput import TextInput
from kivy.uix.videoplayer import VideoPlayer
from openpyxl import load_workbook, Workbook

configFile = "bvo_conf.json"

label_options = ["Following", "Sniff Head", "Sniff Geni", "Playing"]


class BehaviourCategory(BoxLayout):
    name = StringProperty("")
    count = StringProperty("0")
    time = StringProperty("0")
    button = None
    start_time = 0

    def __init__(self, **kwargs):
        super(BehaviourCategory, self).__init__(orientation='vertical')
        self.name = kwargs.get("name")
        self.button = Button(text=self.name)
        self.button.background_color = (1, 0, 0, .6)
        self.c_label = Label(text=self.count)
        self.t_label = Label(text=self.time)
        self.bind(count=self.c_label.setter('text'))
        self.bind(time=self.t_label.setter('text'))
        self.add_widget(self.button)
        self.add_widget(self.c_label)
        self.add_widget(self.t_label)

    def measure(self, *args):
        if self.start_time == 0:
            self.count = str(int(self.count) + 1)
            self.start_time = time.time()
            self.button.background_color = (0, 1, 0, .6)
        else:
            self.time = str(round(float(self.time) + (time.time() - self.start_time), 1))
            self.start_time = 0
            self.button.background_color = (1, 0, 0, .6)


class BehaviourVideoObserver(App):
    activeCat = None
    videoPath = ""
    id = ""
    date = ""
    wbPath = "text.xlsx"
    resultPopup = None
    firstRow = ["Date", "Id"]

    def __init__(self):
        super(BehaviourVideoObserver, self).__init__()

        self.leftView = FileChooserListView(path='./', size_hint=(.3, 1.0))
        self.leftView.bind(on_submit=self._on_submit)

        self.showView = VideoPlayer()
        self.showView.bind(state=self.video_done)

        self.userView = BoxLayout(size_hint=(1.0, .2))
        self.resetBtn = Button(text="Reset")
        self.resetBtn.bind(on_press=self.reset)
        # self.addBtn = Button(text="Add")
        # self.addBtn.bind(on_press=self.add)
        # self.closeBtn = Button(text="Close")
        # self.closeBtn.bind(on_press=self.close)
        self.buttons = BoxLayout(orientation='vertical')
        self.buttons.add_widget(self.resetBtn)
        # self.buttons.add_widget(self.addBtn)
        # self.buttons.add_widget(self.closeBtn)
        self.bCats = [
            BehaviourCategory(name=label_option)
            for label_option in label_options
        ]
        for bCat in self.bCats:
            self.userView.add_widget(bCat)
            self.firstRow += [bCat.name+" Count", bCat.name+" Duration"]
        self.userView.add_widget(self.buttons)

        self.rightView = GridLayout(cols=1)
        self.rightView.add_widget(self.showView)
        self.rightView.add_widget(self.userView)

        self.mainView = GridLayout(cols=2)
        self.mainView.add_widget(self.leftView)
        self.mainView.add_widget(self.rightView)

        Window.bind(on_key_down=self._on_key_down)
        Window.bind(on_key_up=self._on_key_up)

        self.wb = Workbook()
        self.ws = self.wb.active
        self.ws.append(self.firstRow)

    def add(self, *args):
        row = [self.id, self.date]
        for bCat in self.bCats:
            row += [bCat.count, bCat.time]
        self.ws.append(row)

    def close(self, *args):
        self.wb.save(self.wbPath)

    def reset(self, *args):
        if self.activeCat is not None:
            self.activeCat.measure()
            self.activeCat = None
        for bCat in self.bCats:
            bCat.count = "0"
            bCat.time = "0"
            bCat.start_time = 0
        self.showView.state = 'pause'
        self.showView.seek(0)

    def video_done(self, *args):
        if args[1] == "stop":
            if self.activeCat is not None:
                self.activeCat.measure()
                self.activeCat = None
            filename = os.path.splitext(os.path.basename(self.videoPath))[0]
            date, unmarked, marked = filename.split('_')
            self.id = marked
            self.date = date[6:8] + "." + date[4:6] + "." + date[0:4]

            results = BoxLayout(size_hint=(1.0, .6))
            for cat in self.bCats:
                catResult = BoxLayout(orientation='vertical')
                catResult.add_widget(Label(text=cat.name))
                catResult.add_widget(Label(text=cat.count))
                catResult.add_widget(Label(text=cat.time))
                results.add_widget(catResult)

            buttons = BoxLayout(size_hint=(1.0, .2))
            rej = Button(text="Reject", size_hint=(.5, 1.0), background_color=(1, 0, 0, .8))
            rej.bind(on_press=self.reject)
            buttons.add_widget(rej)
            ans = Button(text="Accept", size_hint=(.5, 1.0), background_color=(0, 1, 0, .8))
            ans.bind(on_press=self.accept)
            buttons.add_widget(ans)

            content = BoxLayout(orientation='vertical')
            content.add_widget(TextInput(text=self.id, size_hint=(1.0, 0.1)))
            content.add_widget(TextInput(text=self.date, size_hint=(1.0, 0.1)))
            content.add_widget(results)
            content.add_widget(buttons)

            self.resultPopup = Popup(title=self.videoPath, content=content)
            self.resultPopup.open()

    def accept(self, _):
        self.add(_)
        self.close(_)
        self.reset(_)
        self.resultPopup.dismiss()

    def reject(self, _):
        self.reset(_)
        self.resultPopup.dismiss()

    def _on_key_down(self, instance, key, scancode, codepoint, modifiers):
        try:
            if 0 <= int(key)-49 < len(self.bCats):
                if self.activeCat is None:
                    self.activeCat = self.bCats[int(key) - 49]
                    self.activeCat.measure()
                elif self.activeCat != self.bCats[int(key) - 49]:
                    self.activeCat.measure()
                    self.activeCat = self.bCats[int(key) - 49]
                    self.activeCat.measure()
        except IndexError:
            print(f"Die Tastenkombination {modifiers} + {key} kann noch nix! ")

    def _on_key_up(self, instance, key, scancode):
        try:
            if 0 <= int(key) - 49 < len(self.bCats):
                if self.activeCat == self.bCats[int(key) - 49]:
                    self.activeCat.measure()
                    self.activeCat = None
        except IndexError:
            print(f"Die Tastenkombination {key} kann noch nix! ")

    def _on_submit(self, *args):
        if args[1][0].endswith(".xlsx"):
            self.wbPath = args[1][0]
            self.wb = load_workbook(args[1][0])
            self.ws = self.wb.active
        else:
            self.videoPath = args[1][0]
            self.showView.source = args[1][0]
            self.showView.state = 'play'

    def build(self):
        return self.mainView


if __name__ == '__main__':
    # Load configs
    try:
        with open(configFile, "r") as file:
            label_options = json.load(file)
    except FileNotFoundError:
        with open(configFile, "w+") as file:
            json.dump(label_options, file)
    # Start app
    try:
        BehaviourVideoObserver().run()
    except Exception as e:
        print("Something went wrong!")
        raise e

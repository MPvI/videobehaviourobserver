# -*- mode: python -*-
from kivy.deps import sdl2, glew, gstreamer
block_cipher = None
options = [ ('v', None, 'OPTION') ]

a = Analysis(['bvo.py'],
             pathex=['C:\\Users\\Admin\\Git\\videobehaviourobserver'],
             binaries=[],
             datas=[],
             hiddenimports=['win32timezone'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          options,
          *[Tree(p) for p in (sdl2.dep_bins + glew.dep_bins + gstreamer.dep_bins)],
          [],
          name='VideoBehaviourObserver',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=True , icon='bvo.ico')
